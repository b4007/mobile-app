package dam.androidalejandror.batoipop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import dam.androidalejandror.batoipop.Api.ApiGetUsuario;
import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Dao.BackgroundUsuarioDAO;
import dam.androidalejandror.batoipop.Dao.BatoipopUsuarioDAO;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;

public class Login extends AppCompatActivity {

    private BatoipopUsuario user;
    EditText etName, etPass;
    private BackgroundUsuarioDAO bgUser;
    SharedPreferences sharedPreferences;


    private ApiGetUsuario apiGetUsuario = new ApiGetUsuario(new OnTaskCompleted() {
        @Override
        public void onTaskCompleted() {
            user = apiGetUsuario.getUser();
            Toast.makeText(getApplicationContext(),"Bienvenido de nuevo "+user.getNickname(),Toast.LENGTH_SHORT).show();
            GeneralSetings.setUser(user);
            startActivity(new Intent("STARTINGPOINT"));
            finish();
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etName = findViewById(R.id.logName);
        etPass = findViewById(R.id.logPass);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String credentialMail =sharedPreferences.getString("mail", "");
        String credentialPass = sharedPreferences.getString("pass", "");
        int userPreference = sharedPreferences.getInt("user", 0);
        if (credentialMail != "" && credentialPass != "") {
            apiGetUsuario.setMultiSearch(false);
            apiGetUsuario.setMyUrl("http://137.74.226.41:8080/usuario/"+userPreference);
            apiGetUsuario.execute();
        }
    }

    public void performLogin(View view) {
            bgUser = new BackgroundUsuarioDAO(new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {
                    int userID = bgUser.getUserID();
                    if (userID != 0){
                        apiGetUsuario.setMultiSearch(false);
                        apiGetUsuario.setMyUrl("http://137.74.226.41:8080/usuario/"+userID);
                        apiGetUsuario.execute();

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        String textUSer = etName.getText().toString();
                        String textPass = etPass.getText().toString();
                        System.out.println(textUSer);
                        System.out.println(textPass);
                        editor.putString("mail", textUSer);
                        editor.putString("pass", textPass);
                        editor.putInt("user", bgUser.getUserID());
                        editor.apply();
                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),"No se ha encontrado el usuario.\b Prueba de nuevo o registrate!",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });
            bgUser.setMail(etName.getText().toString());
            bgUser.setPswd(etPass.getText().toString());
            bgUser.setContext(this);
            bgUser.execute();
    }

    public void registerUser(View view){
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }
}