package dam.androidalejandror.batoipop;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dam.androidalejandror.batoipop.Api.ApiPost;
import dam.androidalejandror.batoipop.Api.ApiGetUsuario;
import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Dao.BatoipopUsuarioDAO;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;

public class Register extends AppCompatActivity {

    EditText nombre, ape, usernmae, mail, pass;
    ApiPost apiPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setUI();
    }

    private void setUI() {
        nombre = findViewById(R.id.regNombre);
        ape = findViewById(R.id.regApelldos);
        usernmae = findViewById(R.id.regUsername);
        mail = findViewById(R.id.regMail);
        pass = findViewById(R.id.regPasswd);

        Button btnRegister = findViewById(R.id.buttonRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    apiPost = new ApiPost(new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted() {
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog dialog = new AlertDialog.Builder(Register.this)
                                            .setMessage("Enhorabuena! Tu usario ha sido registrado \nPrueba a iniciar sesión!")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
//                                            getApplicationContext().startActivity(new Intent(getApplicationContext(), Login.class));
                                                    finish();
                                                }
                                            }).create();
                                    dialog.show();
                                }
                            });
                        }
                    });


                    if (nombre.getText().toString().equals("") || ape.getText().toString().equals("") || usernmae.getText().toString().equals("") || mail.getText().toString().equals("") || pass.getText().toString().equals("")){
                        Toast.makeText(getApplicationContext(), "Rellena todos los campos",Toast.LENGTH_SHORT).show();
                    }else {
                        BatoipopUsuario user = new BatoipopUsuario(nombre.getText().toString(),ape.getText().toString(),usernmae.getText().toString(),mail.getText().toString(),pass.getText().toString());
                        apiPost.setContext(Register.this);
                        apiPost.setUsuario(user);
                        apiPost.execute();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}