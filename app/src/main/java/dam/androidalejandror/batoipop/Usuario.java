package dam.androidalejandror.batoipop;

import java.io.Serializable;

public class Usuario implements Serializable {
    private String nombre;
    private int imagen;
    private String mensaje;

    public Usuario(String nombre, int imagen, String mensaje) {
        this.nombre = nombre;
        this.imagen = imagen;
        this.mensaje = mensaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
