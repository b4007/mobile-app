package dam.androidalejandror.batoipop.Pojos;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final

public class BatoipopUsuario {

    private int id;
	private String nombre;
	private String apellidos;
	private String nickname;
	private String email;
	private String contraseña;
	private String avatar;

	public BatoipopUsuario() {
	}

	public BatoipopUsuario(int id, String nombre, String email, String contraseña) {
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.contraseña = contraseña;
	}

	public BatoipopUsuario(int id, String nombre, String apellidos, String nickname, String email, String contraseña) {
		this.id = id;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.nickname = nickname;
		this.email = email;
		this.contraseña = contraseña;
	}

	public BatoipopUsuario(String nombre, String apellidos, String nickname, String email, String contraseña) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.nickname = nickname;
		this.email = email;
		this.contraseña = contraseña;
	}

	public BatoipopUsuario(String nombre, String apellidos, String nickname, String email, String contraseña, String avatar) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.nickname = nickname;
		this.email = email;
		this.contraseña = contraseña;
		this.avatar = avatar;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNickname() {
		return this.nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContraseña() {
		return this.contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	@Override
	public String toString() {
		return "BatoipopUsuario{" +
				"id=" + id +
				", nombre='" + nombre + '\'' +
				", apellidos='" + apellidos + '\'' +
				", nickname='" + nickname + '\'' +
				", email='" + email + '\'' +
				", contraseña='" + contraseña + '\'' +
				'}';
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
}
