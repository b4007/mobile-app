package dam.androidalejandror.batoipop.Pojos;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final




public class BatoipopCategoria implements java.io.Serializable{
	private Integer id;
	private String nombre;
	private String descripcion;

	public BatoipopCategoria(Integer id, String nombre, String descripcion) {
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public BatoipopCategoria( String nombre, String descripcion) {
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public BatoipopCategoria() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return nombre;
	}
}
