package dam.androidalejandror.batoipop.Pojos;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final

/**
 * BatoipopDenunciaa generated by hbm2java
 */
public class BatoipopDenunciaa implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BatoipopDenunciaaId id;

	public BatoipopDenunciaa() {
	}

	public BatoipopDenunciaa(BatoipopDenunciaaId id) {
		this.id = id;
	}

	public BatoipopDenunciaaId getId() {
		return this.id;
	}

	public void setId(BatoipopDenunciaaId id) {
		this.id = id;
	}

}
