package dam.androidalejandror.batoipop.Pojos;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final

public class BatoipopEtiqueta implements java.io.Serializable {

    private int id;
    private String nombre;

    public BatoipopEtiqueta() {
    }

    public BatoipopEtiqueta(int id) {
        this.id = id;
    }

    public BatoipopEtiqueta(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
