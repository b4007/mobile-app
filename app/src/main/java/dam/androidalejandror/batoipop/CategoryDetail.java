package dam.androidalejandror.batoipop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;

import dam.androidalejandror.batoipop.Adapters.MyAdapter;
import dam.androidalejandror.batoipop.Adapters.MyAdapterFav;
import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Dao.SearchByCategory;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopCategoria;

public class CategoryDetail extends AppCompatActivity implements MyAdapter.OnItemClick{

    RecyclerView rv;
    MyAdapter myAdapter;
    BatoipopCategoria categoria;
    private ArrayList<BatoipopArticulo> articulosCategoria = new ArrayList<>();
    private Context context = this;
    SearchByCategory sbc = new SearchByCategory(getApplication(), new OnTaskCompleted() {
        @Override
        public void onTaskCompleted() {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    articulosCategoria.addAll(sbc.getListCateg());
                    for (BatoipopArticulo cat : articulosCategoria){
                        System.out.println(cat);
                    }
                    myAdapter.notifyDataSetChanged();
                }
            });

        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);

        setUI();
    }

    private void setUI() {
        Intent intent = getIntent();
        categoria = (BatoipopCategoria) intent.getSerializableExtra("categ");
        rv = findViewById(R.id.rvCategProducts);
        myAdapter = new MyAdapter(articulosCategoria,this, this);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new GridLayoutManager(this,2));
        rv.setAdapter(myAdapter);

        sbc.setContext(this);
        sbc.setCateg(categoria);
        sbc.execute();
    }


    @Override
    public void onDataClick(BatoipopArticulo prod) {

    }
}