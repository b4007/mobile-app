package dam.androidalejandror.batoipop.Screens;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import dam.androidalejandror.batoipop.Adapters.MyAdapter;
import dam.androidalejandror.batoipop.Adapters.MyAdapterCategories;
import dam.androidalejandror.batoipop.Adapters.MyAdapterChat;
import dam.androidalejandror.batoipop.Chat;
import dam.androidalejandror.batoipop.Product;
import dam.androidalejandror.batoipop.R;

public class ChatScreen extends AppCompatActivity implements MyAdapterChat.OnItemClick, View.OnClickListener {

    RecyclerView rvMessages;
    MyAdapterChat myAdapter;
    private ArrayList<Chat> msgList = new ArrayList<Chat>();
    private Toolbar toolbar;
    private ImageView send;
    private EditText msgBox;

    PrintWriter pw;
    BufferedReader br;
    int id = 2; //CAMBIAR PARA QUE SEA DINAMICO POR MEDIO DE JDBC O API
    Socket socket = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_screen);
        rvMessages = findViewById(R.id.rvMsg);
        myAdapter = new MyAdapterChat(msgList,this);
        rvMessages.setAdapter(myAdapter);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            setUI();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void setUI() throws IOException {
        toolbar = findViewById(R.id.chatTopBar);
        setSupportActionBar(toolbar);

        send = findViewById(R.id.sendButton);
        msgBox = findViewById(R.id.msgBox);


        ImageView back = findViewById(R.id.backArrow);


        do {
            socket = new Socket("137.74.226.41", 9090);
        } while (!socket.isConnected());

        pw = new PrintWriter(socket.getOutputStream(), true);
        br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        pw.println(id);
        while (br.readLine() == null){}
        pw.println(1);
        
        rvMessages = findViewById(R.id.rvMsg);
        rvMessages.setHasFixedSize(true);
        rvMessages.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                do {
                    readLines();
                } while (socket.isConnected());
            }
        });
        thread1.start();

        send.setOnClickListener(this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void readLines() {
        String line = "";
        try {
            if ((line = br.readLine()) != null) {
                Handler handler = new Handler(Looper.getMainLooper());
                String finalLine = line;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        msgList.add(new Chat(2,finalLine));
                        myAdapter.notifyDataSetChanged();
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                pw.println(msgBox.getText().toString());
                msgList.add(new Chat(1,msgBox.getText().toString()));
                msgBox.setText("");

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        myAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
        thread.start();
    }

    @Override
    public void onDataClick(Chat c) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}