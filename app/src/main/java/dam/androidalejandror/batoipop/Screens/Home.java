package dam.androidalejandror.batoipop.Screens;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dam.androidalejandror.batoipop.Adapters.MyAdapter;
import dam.androidalejandror.batoipop.Adapters.MyAdapterCategories;
import dam.androidalejandror.batoipop.Api.ApiArticulo;
import dam.androidalejandror.batoipop.Api.ApiCategoria;
import dam.androidalejandror.batoipop.Api.ApiGetUsuario;
import dam.androidalejandror.batoipop.Api.OdooSessionID;
import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Categoria;
import dam.androidalejandror.batoipop.CategoryDetail;
import dam.androidalejandror.batoipop.GeneralSetings;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopCategoria;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;
import dam.androidalejandror.batoipop.Product;
import dam.androidalejandror.batoipop.ProductDetail;
import dam.androidalejandror.batoipop.R;
import dam.androidalejandror.batoipop.Register;


public class Home extends Fragment implements MyAdapter.OnItemClick, MyAdapterCategories.OnItemClick, MyAdapter.OnFavClick {

    RecyclerView rvProducts;
    RecyclerView rvCategories;
    MyAdapter myAdapter;
    String header;
    MyAdapterCategories myAdapterCategories;
    Gson gson = new Gson();
    private String URL = "http://137.74.226.41:8069/web/image/?model=batoipop.articulo&field=foto&";
    private String URLUSER = "http://137.74.226.41:8069/web/image/?model=batoipop.usuario&field=foto&";

    private ApiGetUsuario apiGetUsuario = new ApiGetUsuario(new OnTaskCompleted() {
        @Override
        public void onTaskCompleted() {
            userList = apiGetUsuario.getUserList();
            for (BatoipopUsuario u: userList){
                String fotoUrl = URLUSER+header+"&id="+u.getId();
                u.setAvatar(fotoUrl);
            }
            GeneralSetings.getUser().setAvatar(URLUSER+header+"&id="+GeneralSetings.getUser().getId());
            GeneralSetings.setUsuarios(userList);
        }
    });

    private ApiArticulo apiArticulo = new ApiArticulo(new OnTaskCompleted() {
        @Override
        public void onTaskCompleted() {
            productList = apiArticulo.getBatoiArticulos();
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    header = odoo.getSessionID();
                    for (BatoipopArticulo art : productList){
                        String fotoUrl = URL+header+"&id="+art.getId();
                        art.setFoto(fotoUrl);
                    }
                    GeneralSetings.setArticulos(productList);
                    fetchImagesUser();
                    myAdapter.notifyDataSetChanged();
                }
            });
        }
    });
    private ApiCategoria apiCategoria = new ApiCategoria();
    private OdooSessionID odoo = new OdooSessionID(new OnTaskCompleted() {
        @Override
        public void onTaskCompleted() {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    fetchData();
                    myAdapter.notifyDataSetChanged();
                }
            });
        }
    });

    private ArrayList<BatoipopArticulo> productList = new ArrayList<>();
    private ArrayList<BatoipopUsuario> userList = new ArrayList<>();
    private ArrayList<BatoipopCategoria> categoryList = new ArrayList<>();
    private ArrayList<BatoipopArticulo> favList = new ArrayList<>();
    SharedPreferences sharedPreferences;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public Home() {
        // Required empty public constructor
    }

    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        fetchImages();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        myAdapter = new MyAdapter(productList, this, this, getContext());
        myAdapterCategories = new MyAdapterCategories(categoryList, this);
        fetchCategories();
        setUI();
    }


    private void setUI() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.persian_green));
        }
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        rvProducts = view.findViewById(R.id.rvProductList);
        rvProducts.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rvProducts.setAdapter(myAdapter);

        rvCategories = view.findViewById(R.id.rvCategories);
        rvCategories.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvCategories.setAdapter(myAdapterCategories);

        EditText searchBar = view.findViewById(R.id.searchFilter);
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    public void filter(String text){
        ArrayList<BatoipopArticulo> filtered = new ArrayList<>();
        for (BatoipopArticulo art: productList){
            if (art.getNombre().toLowerCase().contains(text.toLowerCase())) {
                filtered.add(art);
            }
        }
        myAdapter.filterList(filtered);
    }


    @Override
    public void onDataClick(BatoipopArticulo prod) {
        Intent intent = new Intent(getActivity(), ProductDetail.class);
        intent.putExtra("product", prod);
        startActivity(intent);
    }

    public void fetchData() {
        apiArticulo.setMyUrl("http://137.74.226.41:8080/articulo");
        apiArticulo.setMultiSearch(true);
        apiArticulo.setAdapter(myAdapter);
        apiArticulo.setProducts(productList);
        apiArticulo.setContext(getContext());
        apiArticulo.execute();
    }

    public void fetchCategories(){
        apiCategoria.setMyUrl("http://137.74.226.41:8080/categoria");
        apiCategoria.setMultiSearch(true);
        apiCategoria.setAdapter(myAdapterCategories);
        apiCategoria.setCategorias(categoryList);
        apiCategoria.setContext(getContext());
        apiCategoria.execute();
        categoryList = apiCategoria.getBatoiCategorias();
    }

    public void fetchImages(){
        odoo.setContext(getContext());
        odoo.execute();
    }

    public void fetchImagesUser(){
        apiGetUsuario.setMultiSearch(true);
        apiGetUsuario.setMyUrl("http://137.74.226.41:8080/usuario");
        apiGetUsuario.execute();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString("fav","");
        Type type = new TypeToken<ArrayList<BatoipopArticulo>>() {}.getType();
        List<BatoipopArticulo> list = gson.fromJson(jsonString, type);
        if (jsonString != ""){
            favList.addAll(list);
        }
    }

    @Override
    public void onFavClick(BatoipopArticulo prod) {
        favList.add(prod);
        String jsonString = gson.toJson(favList);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("fav", jsonString);
        editor.apply();
    }

    @Override
    public void onDataClick(BatoipopCategoria categ) {
        System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
        Intent intent = new Intent(getActivity(), CategoryDetail.class);
        intent.putExtra("categ", categ);
        startActivity(intent);
    }
}