package dam.androidalejandror.batoipop.Screens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import dam.androidalejandror.batoipop.Adapters.MyAdapterFav;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Product;
import dam.androidalejandror.batoipop.ProductDetail;
import dam.androidalejandror.batoipop.R;


public class Favorites extends Fragment implements MyAdapterFav.OnItemClick, MyAdapterFav.OnFavClick {

    RecyclerView rvFavProd;
    MyAdapterFav myAdapter;
    Context context;
    View root;
    SharedPreferences sharedPreferences;

    private ImageView imgEmpty;
    private TextView tvEmpty;

    private ArrayList<BatoipopArticulo> productList = new ArrayList<BatoipopArticulo>();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public Favorites() {
        // Required empty public constructor
    }

    public static Favorites newInstance(String param1, String param2) {
        Favorites fragment = new Favorites();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        myAdapter = new MyAdapterFav(productList,this, this,getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_favorites, container, false);
        rvFavProd = root.findViewById(R.id.rvFavProducts);
        rvFavProd.setLayoutManager(new GridLayoutManager(getContext(),2));
        rvFavProd.setAdapter(myAdapter);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.persian_green));
        }
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvEmpty = view.findViewById(R.id.favEmpty);
        imgEmpty = view.findViewById(R.id.favEmptyImg);
        imgEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDataClick(BatoipopArticulo prod) {
        Intent intent = new Intent(getActivity(), ProductDetail.class);
        intent.putExtra("product",prod);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString("fav","");
        Type type = new TypeToken<ArrayList<BatoipopArticulo>>() {}.getType();
        List<BatoipopArticulo> list = gson.fromJson(jsonString, type);
        if (list.size() > 0 ){
            imgEmpty.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.GONE);
            productList.addAll(list);
            myAdapter.notifyDataSetChanged();
        }else {
            imgEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFav(BatoipopArticulo prod, int position) {
        Gson gson = new Gson();
        productList.remove(position);
        myAdapter.notifyDataSetChanged();
        if (productList.size() == 0){
            imgEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.VISIBLE);
        }
        String jsonString = gson.toJson(productList);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("fav", jsonString);
        editor.apply();
    }
}