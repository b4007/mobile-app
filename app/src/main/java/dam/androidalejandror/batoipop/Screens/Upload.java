package dam.androidalejandror.batoipop.Screens;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.chip.ChipDrawable;

import dam.androidalejandror.batoipop.Api.ApiArticulo;
import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Dao.BackgroundDAO;
import dam.androidalejandror.batoipop.Dao.BatoipopArticuloDAO;
import dam.androidalejandror.batoipop.GeneralSetings;
import dam.androidalejandror.batoipop.MainActivity;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.R;

public class Upload extends Fragment implements OnMapReadyCallback {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String MAPVIEW_BUNDLE_KEY = "MAPVIEW_BUNDLE_KEY";

    public static final int PICK_IMAGE = 1;
    private int SpannedLength = 0,chipLength = 4;

    private String mParam1;
    private String mParam2;

    public Upload() {

    }

    ImageView imgUpload;
    Spinner spinnerCategories, spinnerEstados;
    EditText etNombre, etDesc, etAnt, etPrecio;
    AppCompatEditText etTags;
    Button prodSubir;
    MapView mapView;


    public static Upload newInstance(String param1, String param2) {
        Upload fragment = new Upload();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.charcoal));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        etNombre = view.findViewById(R.id.prodNombre);
        etDesc = view.findViewById(R.id.prodDesc);
        etPrecio = view.findViewById(R.id.prodPrecio);
        mapView = view.findViewById(R.id.mapView2);


        imgUpload = view.findViewById(R.id.imgUpload);
        imgUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageFromGallery();
            }
        });

        Bundle mapBundle = null;
        if (savedInstanceState != null){
            mapBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        initGoogleMaps(mapBundle);

        prodSubir = view.findViewById(R.id.prodSubir);
        prodSubir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ApiArticulo.PostArticulo postArticulo = new ApiArticulo.PostArticulo();
                    BatoipopArticulo art = new BatoipopArticulo(spinnerCategories.getSelectedItemPosition()+1,GeneralSetings.getUser().getId(),etNombre.getText().toString(),Double.parseDouble(etPrecio.getText().toString()),spinnerEstados.getSelectedItem().toString(),etDesc.getText().toString(),true);

                    BackgroundDAO bgDao = new BackgroundDAO(getContext(), new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted() {
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    showDialog();
                                }
                            });
                        }
                    });
                    bgDao.setArt(art);
                    bgDao.execute();

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        spinnerCategories = view.findViewById(R.id.prodCategoria);
        ArrayAdapter adapterCategories = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, GeneralSetings.getCategorias());
        adapterCategories.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategories.setAdapter(adapterCategories);

        spinnerEstados = view.findViewById(R.id.prodEstado);
        ArrayAdapter<CharSequence> adapterEstados = ArrayAdapter.createFromResource(getContext(), R.array.Estado, android.R.layout.simple_spinner_item);
        adapterEstados.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEstados.setAdapter(adapterEstados);

        etTags = view.findViewById(R.id.prodTags);

        etTags.addTextChangedListener(new TextWatcher() {
            private int lastLength;
            boolean nextWord;
            ChipDrawable chip;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                int previousWordLenght = 0;
                try {
                    String lastChar = s.toString().substring(s.length() - 1);
                    if (lastChar.equals(" ")) {
                        nextWord = true;
                        previousWordLenght = s.length();
                    }
                    if (s.length() < previousWordLenght){
                        s.toString().substring(s.length() - previousWordLenght);
                    }
                }catch (Exception e){}
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if(nextWord) {
                        chip = ChipDrawable.createFromResource(getContext(), R.xml.chip);
                        chip.setText(editable.subSequence(SpannedLength,editable.length()));
                        chip.setBounds(0, 0, chip.getIntrinsicWidth(), chip.getIntrinsicHeight());
                        ImageSpan span = new ImageSpan(chip);
                        editable.setSpan(span, SpannedLength, editable.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        SpannedLength = editable.length();
                        nextWord = false;
                    }
                }catch (Exception e){}
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    private void initGoogleMaps(Bundle mapBundle) {
        mapView.onCreate(mapBundle);
        mapView.getMapAsync(this);
    }

    private void selectImageFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(i, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE) {
            if (data != null){
                Uri uri = data.getData();
                imgUpload.setImageURI(uri);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_upload, container, false);
    }

    private void showDialog(){
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setMessage("El articulo se ha registrado!")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();

        dialog.show();

        etNombre.setText("");
        etDesc.setText("");
        etPrecio.setText("");
        etTags.setText("");
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions().position(new LatLng(38.70545,-0.47432)).title("Marcador"));
    }

    @Override
    public void onStart(){
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop(){
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause(){
        super.onPause();
        mapView.onPause();
    }
    @Override
    public void onResume(){
        super.onResume();
        mapView.onResume();
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
            mapView.onSaveInstanceState(mapViewBundle);
        }
    }
}