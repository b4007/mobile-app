package dam.androidalejandror.batoipop.Screens;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;

import dam.androidalejandror.batoipop.Adapters.MyAdapter;
import dam.androidalejandror.batoipop.Adapters.MyAdapterInbox;
import dam.androidalejandror.batoipop.Product;
import dam.androidalejandror.batoipop.ProductDetail;
import dam.androidalejandror.batoipop.R;
import dam.androidalejandror.batoipop.Usuario;

public class Inbox extends Fragment implements MyAdapterInbox.OnItemClick{

    RecyclerView rvInbox;
    MyAdapterInbox myAdapter;
    Context context;
    View root;
    private ArrayList<Usuario> userList = new ArrayList<Usuario>();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    public Inbox() {
        // Required empty public constructor
    }

    public static Inbox newInstance(String param1, String param2) {
        Inbox fragment = new Inbox();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        myAdapter = new MyAdapterInbox(crearLista(),this, getContext());
    }

    private ArrayList<Usuario> crearLista() {
        Usuario user = new Usuario("Usuario", R.drawable.prod,"Ultimo mensaje");
        for (int i = 0; i < 30; i++) {
            userList.add(user);
        }
        //TODO: API REQUEST -> GET
        return userList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_inbox, container, false);
        rvInbox = root.findViewById(R.id.rvInbox);
        rvInbox.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL, false));
        rvInbox.setAdapter(myAdapter);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.charcoal));
        }
        return root;
    }

    @Override
    public void onDataClick(Usuario user) {
        Intent intent = new Intent(getActivity(), ChatScreen.class);
//        intent.putExtra("user",user);
        startActivity(intent);
    }
}