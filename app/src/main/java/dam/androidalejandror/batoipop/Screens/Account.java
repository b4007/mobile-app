package dam.androidalejandror.batoipop.Screens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;

import dam.androidalejandror.batoipop.Adapters.MyAdapter;
import dam.androidalejandror.batoipop.Adapters.MyAdapterFav;
import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Dao.MyProductsDAO;
import dam.androidalejandror.batoipop.GeneralSetings;
import dam.androidalejandror.batoipop.Login;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;
import dam.androidalejandror.batoipop.ProductDetail;
import dam.androidalejandror.batoipop.R;

public class Account extends Fragment implements MyAdapter.OnItemClick, MyAdapter.OnFavClick{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private BatoipopUsuario user = GeneralSetings.getUser();
    private TextView accName;
    private ImageView accImg, heart;
    private RatingBar rating;
    private Button close;

    RecyclerView rvMyProd;
    MyAdapter myAdapter;
    Context context;
    View root;
    MyProductsDAO dao;

    private ArrayList<BatoipopArticulo> myProds = new ArrayList<>();

    public Account() {
        // Required empty public constructor
    }


    public static Account newInstance(String param1, String param2) {
        Account fragment = new Account();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        myAdapter = new MyAdapter(myProds,this, getContext());
        listMyProds();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_account, container, false);
        rvMyProd = root.findViewById(R.id.rvMyProds);
        rvMyProd.setLayoutManager(new GridLayoutManager(getContext(),2));
        rvMyProd.setAdapter(myAdapter);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.persian_green));
        }
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        accName = view.findViewById(R.id.accUsername);
        accName.setText(user.getNickname());
        accImg = view.findViewById(R.id.accImage);
        close = view.findViewById(R.id.close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                preferences.edit().remove("mail").apply();
                preferences.edit().remove("pass").apply();
                preferences.edit().remove("user").apply();
                startActivity(new Intent(getContext(), Login.class));
                getActivity().finish();
            }
        });

        Glide.with(getActivity()).asBitmap().error(R.drawable.batoilogo).load(GeneralSetings.getUser().getAvatar()).into(accImg);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDataClick(BatoipopArticulo prod) {
        Intent intent = new Intent(getActivity(), ProductDetail.class);
        intent.putExtra("product",prod);
        startActivity(intent);
    }

    public void listMyProds(){
        dao = new MyProductsDAO(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        myProds.addAll(dao.getList());
                        myAdapter.notifyDataSetChanged();
                    }
                });

            }
        });

        dao.setContext(getContext());
        dao.setVendedor(GeneralSetings.getUser().getId());
        dao.execute();
    }

    @Override
    public void onFavClick(BatoipopArticulo prod) {

    }
}