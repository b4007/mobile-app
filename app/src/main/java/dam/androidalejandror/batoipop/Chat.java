package dam.androidalejandror.batoipop;

public class Chat {
    private int id;
    private String mensaje;

    public Chat(int id, String mensaje) {
        this.id = id;
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
