package dam.androidalejandror.batoipop;

import java.util.ArrayList;

import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopCategoria;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;

public class GeneralSetings {
    private static BatoipopUsuario user;
    private static ArrayList<BatoipopCategoria> categorias;
    private static ArrayList<BatoipopUsuario> usuarios;
    private static ArrayList<BatoipopArticulo> articulos;

    public static BatoipopUsuario getUser() {
        return user;
    }

    public static void setUser(BatoipopUsuario user) {
        GeneralSetings.user = user;
    }

    public static ArrayList<BatoipopCategoria> getCategorias() {
        return categorias;
    }

    public static void setCategorias(ArrayList<BatoipopCategoria> categorias) {
        GeneralSetings.categorias = categorias;
    }

    public static ArrayList<BatoipopUsuario> getUsuarios() {
        return usuarios;
    }

    public static void setUsuarios(ArrayList<BatoipopUsuario> usuarios) {
        GeneralSetings.usuarios = usuarios;
    }

    public static ArrayList<BatoipopArticulo> getArticulos() {
        return articulos;
    }

    public static void setArticulos(ArrayList<BatoipopArticulo> articulos) {
        GeneralSetings.articulos = articulos;
    }
}
