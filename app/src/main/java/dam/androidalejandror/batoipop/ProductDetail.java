package dam.androidalejandror.batoipop;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import dam.androidalejandror.batoipop.Api.ApiGetUsuario;
import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Dao.BuyDAO;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;
import dam.androidalejandror.batoipop.Util.GlideApp;

public class ProductDetail extends AppCompatActivity implements OnMapReadyCallback {

    private TextView tvNombre, tvPrecio, tvDesc, tvEstado, tvUserName, tvMail;
    private Button btnComprar;
    public static final String MAPVIEW_BUNDLE_KEY = "MAPVIEW_BUNDLE_KEY";
    private ImageView image, userImage;
    private MapView mapView;
    private ApiGetUsuario apiGetUsuario = new ApiGetUsuario(new OnTaskCompleted() {
        @Override
        public void onTaskCompleted() {
            BatoipopUsuario vendedor = apiGetUsuario.getUser();
            tvUserName.setText(vendedor.getNickname());
            tvMail.setText(vendedor.getEmail());
            BatoipopUsuario vend = null;
            for (BatoipopUsuario u : GeneralSetings.getUsuarios()) {
                if (u.getNickname().equals(vendedor.getNickname())) {
                    vend = u;
                    break;
                }
            }
            GlideApp.with(getApplication()).asBitmap().error(R.drawable.batoilogo).load(vend.getAvatar()).into(userImage);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prodcut_detail);

        setUI();

        Bundle mapBundle = null;
        if (savedInstanceState != null){
            mapBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        initGoogleMaps(mapBundle);
    }

    private void initGoogleMaps(Bundle mapBundle) {
        mapView.onCreate(mapBundle);
        mapView.getMapAsync(this);
    }

    private void setUI() {
        tvNombre = findViewById(R.id.nameDetail);
        tvPrecio = findViewById(R.id.priceDetail);
        image = findViewById(R.id.imgDetail);
        tvDesc = findViewById(R.id.prodDescDetail);
        tvEstado = findViewById(R.id.estadoDetail);
        tvUserName = findViewById(R.id.userNameDetail);
        userImage = findViewById(R.id.userImageDetail);
        mapView = findViewById(R.id.mapView);
        tvMail = findViewById(R.id.mailDetail);
        btnComprar = findViewById(R.id.comprar);

        BuyDAO buyDAO = new BuyDAO();

        Intent intent = getIntent();
        BatoipopArticulo product = (BatoipopArticulo) intent.getSerializableExtra("product");

        tvPrecio.setText(String.format("€%s", product.getPrecio()));
        tvNombre.setText(product.getNombre());
        tvDesc.setText(product.getDescripcion());
        tvEstado.setText(product.getAntigüedad());
        GlideApp.with(getApplication()).asBitmap().load(product.getFoto()).into(image);

        apiGetUsuario.setMultiSearch(false);
        apiGetUsuario.setMyUrl("http://137.74.226.41:8080/usuario/"+product.getBatoipopUsuarioByVendedor());
        apiGetUsuario.execute();

        Context context = this;
        btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buyDAO.setContext(context);
                product.setBatoipopUsuarioByComprador(GeneralSetings.getUser().getId());
                buyDAO.setArticulo(product);
                buyDAO.execute();
            }
        });
    }



    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions().position(new LatLng(0,0)).title("Marcador"));
    }

    @Override
    public void onStart(){
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop(){
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause(){
        super.onPause();
        mapView.onPause();
    }
    @Override
    public void onResume(){
        super.onResume();
        mapView.onResume();
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
            mapView.onSaveInstanceState(mapViewBundle);
        }
    }
}