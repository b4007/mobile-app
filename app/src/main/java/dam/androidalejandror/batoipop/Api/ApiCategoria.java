package dam.androidalejandror.batoipop.Api;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import dam.androidalejandror.batoipop.Adapters.MyAdapter;
import dam.androidalejandror.batoipop.Adapters.MyAdapterCategories;
import dam.androidalejandror.batoipop.GeneralSetings;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopCategoria;

public class ApiCategoria extends AsyncTask<String, String, String> {

    ProgressDialog progressDialog;
    String myUrl = null;
    boolean multiSearch = false;
    public ArrayList<BatoipopCategoria> batoiCategorias = new ArrayList<>();
    private ArrayList<BatoipopCategoria> categorias;
    private MyAdapterCategories adapter;
    private Context context;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Cargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String result = "";
        try {
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(myUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader isw = new InputStreamReader(in);
                int data = isw.read();
                while (data != -1) {
                    result += (char) data;
                    data = isw.read();

                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Exception: " + e.getMessage();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {

        try {
            if (multiSearch){
                JSONArray jArray = new JSONArray(s);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject item = jArray.getJSONObject(i);
                    BatoipopCategoria cat = new BatoipopCategoria(item.getInt("id"),item.getString("nombre"),item.getString("descripcion"));
                    batoiCategorias.add(cat);
                }
                categorias.addAll(batoiCategorias);
                GeneralSetings.setCategorias(categorias);
                adapter.notifyDataSetChanged();
            }else {
                JSONObject json = new JSONObject(s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            progressDialog.dismiss();
        }
    }

    public void setMyUrl(String myUrl) {
        this.myUrl = myUrl;
    }

    public void setMultiSearch(boolean multiSearch) {
        this.multiSearch = multiSearch;
    }

    public ArrayList<BatoipopCategoria> getBatoiCategorias() {
        return batoiCategorias;
    }

    public void setCategorias(ArrayList<BatoipopCategoria> categorias) {
        this.categorias = categorias;
    }

    public void setAdapter(MyAdapterCategories adapter) {
        this.adapter = adapter;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}
