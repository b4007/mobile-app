package dam.androidalejandror.batoipop.Api;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

import com.google.gson.Gson;

import dam.androidalejandror.batoipop.Dao.BatoipopUsuarioDAO;
import dam.androidalejandror.batoipop.Login;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;

public class ApiPost extends AsyncTask<String, String, String> {

    ProgressDialog progressDialog;
    String myUrl = null;
    Gson gson = new Gson();

    boolean multiSearch = false;
    private BatoipopUsuario usuario;
    private Context context;
    private OnTaskCompleted done;

    public ApiPost(OnTaskCompleted done) {
        this.done = done;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Registrando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String cliJson = gson.toJson(usuario, BatoipopUsuario.class);

        URL url;
        HttpURLConnection con;
        String lin, salida = "";
        try {
            BatoipopUsuarioDAO dao = new BatoipopUsuarioDAO();
            if (dao.canRegister(usuario)){
                url = new URL("http://137.74.226.41:8080/usuario");
                con = (HttpURLConnection) url.openConnection();
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");

                OutputStream os = con.getOutputStream();
                os.write(cliJson.getBytes());
                os.flush();

                if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                    throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                con.disconnect();
                done.onTaskCompleted();
            }else {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Ya existe un usuario registrado con ese correo o nombre de usuario",Toast.LENGTH_SHORT).show();
                    }
                });
            }


        } catch (IOException | SQLException e) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    AlertDialog dialog = new AlertDialog.Builder(context)
                            .setMessage("¡Error al establecer la conexión! \nComprueba tu conexión a internet y vuelve a intentarlo de nuevo!")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
//
                                }
                            }).create();
                    dialog.show();
                }
            });
        }
        return salida;
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();
    }

    public void setUsuario(BatoipopUsuario usuario) {
        this.usuario = usuario;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setMyUrl(String myUrl) {
        this.myUrl = myUrl;
    }

    public void setMultiSearch(boolean multiSearch) {
        this.multiSearch = multiSearch;
    }
}
