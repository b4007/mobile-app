package dam.androidalejandror.batoipop.Api;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

import dam.androidalejandror.batoipop.Dao.BatoipopUsuarioDAO;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;

public class OdooSessionID extends AsyncTask<String, String, String> {

    ProgressDialog progressDialog;
    private Context context;
    private OnTaskCompleted done;
    private String sessionID;

    public OdooSessionID(OnTaskCompleted done) {
        this.done = done;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Cargando imagenes...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        Gson gson = new Gson();
        String textJson = "{\"jsonrpc\": \"2.0\",\"params\": {\"db\": \"batoipop\",\"login\": \"batoipopa@gmail.com\",\"password\": \"Windows2008Server\"}}";
        URL url;
        HttpURLConnection con;
        String lin, salida = "";

        try {
            url = new URL("http://137.74.226.41:8069/web/session/authenticate");
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os = con.getOutputStream();
            os.write(textJson.getBytes());
            os.flush();

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            String header = con.getHeaderField("Set-Cookie");
            String[] sesion = header.split(";");
            sessionID = sesion[0];
            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            while ((lin = br.readLine()) != null) {
                salida = salida.concat(lin);
            }
            con.disconnect();

            done.onTaskCompleted();


        } catch (IOException e) {

        }
        return salida;
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getSessionID() {
        return sessionID;
    }
}
