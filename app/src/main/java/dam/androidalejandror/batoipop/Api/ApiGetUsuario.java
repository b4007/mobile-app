package dam.androidalejandror.batoipop.Api;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;

public class ApiGetUsuario extends AsyncTask<String, String, String>{

    ProgressDialog progressDialog;
    String myUrl = null;
    boolean multiSearch = false;
    private BatoipopUsuario user;
    private ArrayList<BatoipopUsuario> userList = new ArrayList<>();
    private OnTaskCompleted done;
    private Gson gson = new Gson();

    public ApiGetUsuario(OnTaskCompleted done) {
        this.done = done;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        progressDialog = new ProgressDialog(MainActivity.this);
//        progressDialog.setMessage("processing results");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String result = "";
        try {
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(myUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader isw = new InputStreamReader(in);
                int data = isw.read();
                while (data != -1) {
                    result += (char) data;
                    data = isw.read();

                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Exception: " + e.getMessage();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
//        progressDialog.dismiss();

        try {
            if (multiSearch){
                JSONArray jArray = new JSONArray(s);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject item = jArray.getJSONObject(i);
                    BatoipopUsuario u = gson.fromJson(item.toString(),BatoipopUsuario.class);
                    userList.add(u);
                }
                done.onTaskCompleted();
            }else {
                JSONObject json = new JSONObject(s);
                user = new BatoipopUsuario(json.getInt("id"),json.getString("nombre"),json.getString("apellidos"),json.getString("nickname"),json.getString("email"),json.getString("contraseña"));
                done.onTaskCompleted();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void setMyUrl(String myUrl) {
        this.myUrl = myUrl;
    }

    public void setMultiSearch(boolean multiSearch) {
        this.multiSearch = multiSearch;
    }

    public BatoipopUsuario getUser() {
        return user;
    }

    public ArrayList<BatoipopUsuario> getUserList() {
        return userList;
    }
}