package dam.androidalejandror.batoipop.Api;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.gson.Gson;
import android.app.Fragment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import dam.androidalejandror.batoipop.Adapters.MyAdapter;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;
import dam.androidalejandror.batoipop.Screens.Home;

public class ApiArticulo extends AsyncTask<String, String, String> {

    ProgressDialog progressDialog;
    String myUrl = null;
    boolean multiSearch = false;
    public ArrayList<BatoipopArticulo> batoiArticulos = new ArrayList<>();
    private ArrayList<BatoipopArticulo> products;
    private MyAdapter adapter;
    private Context context;
    private OnTaskCompleted done;

    public ApiArticulo(OnTaskCompleted done) {
        this.done = done;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Cargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String result = "";
        try {
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(myUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader isw = new InputStreamReader(in);
                int data = isw.read();
                while (data != -1) {
                    result += (char) data;
                    data = isw.read();

                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Exception: " + e.getMessage();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {

        try {

            if (multiSearch){
                JSONArray jArray = new JSONArray(s);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject item = jArray.getJSONObject(i);
                    BatoipopArticulo art = new BatoipopArticulo(item.getInt("id"),item.getInt("batoipopUsuarioByVendedor"),item.getString("nombre"),item.getDouble("precio"),item.getString("antigüedad"),item.getString("descripcion"));
                    batoiArticulos.add(art);
                }
                products.addAll(batoiArticulos);
                adapter.notifyDataSetChanged();
                done.onTaskCompleted();
            }else {
                JSONObject json = new JSONObject(s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            progressDialog.dismiss();
        }
    }

    public void setMyUrl(String myUrl) {
        this.myUrl = myUrl;
    }

    public void setMultiSearch(boolean multiSearch) {
        this.multiSearch = multiSearch;
    }

    public ArrayList<BatoipopArticulo> getBatoiArticulos() {
        return batoiArticulos;
    }

    public void setAdapter(MyAdapter adapter) {
        this.adapter = adapter;
    }

    public void setProducts(ArrayList<BatoipopArticulo> products) {
        this.products = products;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    // ------------------------------------------------------------------------

    public static class PostArticulo extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        Gson gson = new Gson();

        private BatoipopArticulo articulo;
        private Context context;

        @Override
        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(context);
//            progressDialog.setMessage("Cargando...");
//            progressDialog.setCancelable(false);
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String artJson = gson.toJson(articulo, BatoipopArticulo.class);
            URL url;
            HttpURLConnection con;
            String lin, salida = "";
            try {
                url = new URL("http://137.74.226.41:8080/articulo");
                con = (HttpURLConnection) url.openConnection();
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");

                OutputStream os = con.getOutputStream();
                os.write(artJson.getBytes());
                os.flush();

                if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                    throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                con.disconnect();
            } catch (IOException e) {

            }
            return salida;
        }

        @Override
        protected void onPostExecute(String s) {

        }

        public void setArticulo(BatoipopArticulo articulo) {
            this.articulo = articulo;
        }

        public void setContext(Context context) {
            this.context = context;
        }
    }
}
