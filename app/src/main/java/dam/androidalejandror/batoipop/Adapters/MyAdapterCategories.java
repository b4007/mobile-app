package dam.androidalejandror.batoipop.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidalejandror.batoipop.Categoria;
import dam.androidalejandror.batoipop.Pojos.BatoipopCategoria;
import dam.androidalejandror.batoipop.Product;
import dam.androidalejandror.batoipop.R;

public class MyAdapterCategories extends RecyclerView.Adapter<MyAdapterCategories.MyViewHolder>{
private ArrayList<BatoipopCategoria> categoryList;
    private OnItemClick onItemClick;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNombre;
        ImageView img;

        public MyViewHolder(View view, OnItemClick onItemClick, ArrayList<BatoipopCategoria> products) {
            super(view);
            tvNombre = view.findViewById(R.id.categoryName);
            img = view.findViewById(R.id.categoryImg);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.onDataClick(products.get(getAdapterPosition()));
                }
            });
        }

        public void bind(BatoipopCategoria cat) {
            this.tvNombre.setText(cat.getNombre());
//            this.img.setImageResource(cat.getImagen());
        }
    }
    public MyAdapterCategories(ArrayList<BatoipopCategoria> categorias, OnItemClick onItemClick){
        this.categoryList = categorias;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.categoies_layout, parent, false);
        return new MyViewHolder(itemLayout, onItemClick, categoryList);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind (categoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public interface OnItemClick{
        void onDataClick(BatoipopCategoria prod);
    }
}
