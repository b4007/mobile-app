package dam.androidalejandror.batoipop.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import dam.androidalejandror.batoipop.Chat;
import dam.androidalejandror.batoipop.Product;
import dam.androidalejandror.batoipop.R;

public class MyAdapterChat extends RecyclerView.Adapter<MyAdapterChat.MyViewHolder>{
private ArrayList<Chat> mesagesList;
    private OnItemClick onItemClick;
    private static int LAYOUT_ONE = 0;
    private static int LAYOUT_TWO = 1;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvMensaje;

        public MyViewHolder(View view, OnItemClick onItemClick, ArrayList<Chat> messages) {
            super(view);
            tvMensaje = view.findViewById(R.id.chatMsg);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.onDataClick(messages.get(getAdapterPosition()));
                }
            });

        }

        public void bind(Chat c) {
            this.tvMensaje.setText(c.getMensaje());
        }
    }
    public MyAdapterChat(ArrayList<Chat> mesagesList, OnItemClick onItemClick){
        this.mesagesList = mesagesList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType)
        {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_sender_layout, parent, false);
                return new MyViewHolder(view, onItemClick, mesagesList);

            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_reciever_layout, parent, false);
                return new MyViewHolder(view, onItemClick, mesagesList);

        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (mesagesList.get(position).getId() == 1)
            return LAYOUT_ONE;
        else
            return LAYOUT_TWO;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind (mesagesList.get(position));
    }

    @Override
    public int getItemCount() {
        return mesagesList.size();
    }

    public interface OnItemClick{
        void onDataClick(Chat c);
    }
}
