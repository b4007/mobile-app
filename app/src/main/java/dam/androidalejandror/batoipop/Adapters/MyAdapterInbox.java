package dam.androidalejandror.batoipop.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidalejandror.batoipop.R;
import dam.androidalejandror.batoipop.Usuario;

public class MyAdapterInbox extends RecyclerView.Adapter<MyAdapterInbox.MyViewHolder>{
private ArrayList<Usuario> userList;
    private OnItemClick onItemClick;
    private Context context;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNombre, tvMensaje;
        ImageView img;
        Context cont;

        public MyViewHolder(View view, OnItemClick onItemClick, ArrayList<Usuario> usuarios, Context cont) {
            super(view);
            tvNombre = view.findViewById(R.id.tvNombreUser);
            img = view.findViewById(R.id.userImageDetail);
            tvMensaje = view.findViewById(R.id.tvMensajeUser);
            this.cont = cont;

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.onDataClick(usuarios.get(getAdapterPosition()));
                }
            });
        }

        public void bind(Usuario user) {
            this.tvNombre.setText(user.getNombre());
            this.img.setImageResource(user.getImagen());
            this.tvMensaje.setText(user.getMensaje());
        }
    }
    public MyAdapterInbox(ArrayList<Usuario> userList, OnItemClick onItemClick, Context context){
        this.userList = userList;
        this.onItemClick = onItemClick;
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.inbox_layout, parent, false);
        return new MyViewHolder(itemLayout, onItemClick, userList, context);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind (userList.get(position));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public interface OnItemClick{
        void onDataClick(Usuario user);
    }
}
