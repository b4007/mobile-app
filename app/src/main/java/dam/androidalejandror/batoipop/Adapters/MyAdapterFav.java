package dam.androidalejandror.batoipop.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Product;
import dam.androidalejandror.batoipop.R;
import dam.androidalejandror.batoipop.Util.GlideApp;

public class MyAdapterFav extends RecyclerView.Adapter<MyAdapterFav.MyViewHolder>{
private ArrayList<BatoipopArticulo> productList;
    private OnItemClick onItemClick;
    private OnFavClick onFavClick;
    private Context context;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNombre, tvMensaje;
        ImageView img, heart;
        Context cont;

        public MyViewHolder(View view, OnItemClick onItemClick, OnFavClick onFavClick,ArrayList<BatoipopArticulo> products, Context cont) {
            super(view);
            tvNombre = view.findViewById(R.id.tvNombreFav);
            img = view.findViewById(R.id.idImagen);
            tvMensaje = view.findViewById(R.id.tvPrecioFav);
            heart = view.findViewById(R.id.fav_buttonFav);
            this.cont = cont;

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.onDataClick(products.get(getAdapterPosition()));
                }
            });
            heart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    heart.setImageResource(R.drawable.ic_fav);
                    onFavClick.onFav(products.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }

        public void bind(BatoipopArticulo prod) {
            this.tvNombre.setText(prod.getNombre());
            GlideApp.with(cont).load(prod.getFoto()).into(img);
            this.tvMensaje.setText("€"+prod.getPrecio());
        }
    }
    public MyAdapterFav(ArrayList<BatoipopArticulo> productList, OnItemClick onItemClick, OnFavClick onFavClick,Context context){
        this.productList = productList;
        this.onItemClick = onItemClick;
        this.onFavClick = onFavClick;
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.fav_layout, parent, false);
        return new MyViewHolder(itemLayout, onItemClick, onFavClick,productList, context);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind (productList.get(position));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public interface OnItemClick{
        void onDataClick(BatoipopArticulo prod);
    }

    public interface OnFavClick{
        void onFav(BatoipopArticulo prod, int position);
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
