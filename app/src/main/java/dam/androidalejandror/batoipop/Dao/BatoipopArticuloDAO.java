package dam.androidalejandror.batoipop.Dao;



import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopCategoria;

public class BatoipopArticuloDAO implements GenericDAO<BatoipopArticulo> {

    final String SQLSELECTALL = "SELECT * FROM articulos";
    final String SQLSELECTNOMBRE = "SELECT * FROM batoipop_articulo WHERE vendedor = ?";
    final String SQLSELECTPK = "SELECT * FROM articulos WHERE id = ?";
    final String SQLINSERT = "INSERT INTO batoipop_articulo (categoria_id, vendedor, nombre, precio, antigüedad, descripcion) VALUES (?,?,?,?,?,?)";
    final String SQLUPDATE = "UPDATE batoipop_articulo SET comprador = ?";
    final String SQLDELETE = "DELETE FROM articulos WHERE id = ?";
    final String SQLCAT = "SELECT * FROM batoipop_articulo WHERE categoria_id = ?";

    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectNombre;
    private final PreparedStatement pstSelectAll;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstInsertGenKey;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;
    private final PreparedStatement pstCategory;


//    static {
//        try {
//            grpdao = new GrupoDAOImpl();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

    public BatoipopArticuloDAO() throws SQLException {
        Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectNombre = con.prepareStatement(SQLSELECTNOMBRE);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT);
        pstInsertGenKey = con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);
        pstCategory = con.prepareStatement(SQLCAT);
    }

    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    public ArrayList<BatoipopArticulo> findByCat(BatoipopCategoria categ) throws  SQLException{
        ArrayList<BatoipopArticulo> artByCateg = new ArrayList<>();
        pstCategory.setInt(1,categ.getId());
        ResultSet rs = pstCategory.executeQuery();
        while (rs.next()){
            artByCateg.add(new BatoipopArticulo(rs.getInt("vendedor"),rs.getString("nombre"),rs.getDouble("precio"),rs.getString("antigüedad"),rs.getString("descripcion")));
        }
        return artByCateg;
    }

    @Override
    public BatoipopArticulo findByPK(int id) throws SQLException {
        return null;
    }

    public ArrayList<BatoipopArticulo> findByNombre(int text) throws SQLException {
        pstSelectNombre.setInt(1,text);
        ResultSet rs = pstSelectNombre.executeQuery();
        ArrayList<BatoipopArticulo> myArticulos = new ArrayList<>();
        while (rs.next()){
            myArticulos.add(new BatoipopArticulo(rs.getInt("vendedor"),rs.getString("nombre"),rs.getDouble("precio"),rs.getString("antigüedad"),rs.getString("descripcion")));
        }
        return myArticulos;
    }

    @Override
    public List<BatoipopArticulo> findAll() throws SQLException {
        return null;
    }

    @Override
    public boolean insert(BatoipopArticulo artInsertar) throws SQLException {
        pstInsert.setInt(1, artInsertar.getBatoipopCategoria());
        pstInsert.setInt(2, artInsertar.getBatoipopUsuarioByVendedor());
        pstInsert.setString(3, artInsertar.getNombre());
        pstInsert.setDouble(4, artInsertar.getPrecio());
        pstInsert.setString(5, artInsertar.getAntigüedad());
        pstInsert.setString(6, artInsertar.getDescripcion());
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    public BatoipopArticulo insertGenKey(BatoipopArticulo artInsertar) throws SQLException{
       return null;
    }

    @Override
    public boolean update(BatoipopArticulo artUpdate) throws SQLException {
        pstUpdate.setInt(1, artUpdate.getBatoipopUsuarioByComprador());
        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);
    }

    @Override
    public boolean delete(int id) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(BatoipopArticulo artDelete) throws SQLException {
        return this.delete(artDelete.getId());
    }

    @Override
    public int size() throws SQLException {
        return 0;
    }

    @Override
    public boolean exists(int id) throws SQLException {
        if (findByPK(id) != null){
			return true;
		}
		return false;
    }


}
