package dam.androidalejandror.batoipop.Dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopCategoria;

public class SearchByCategory  extends AsyncTask<String, String, String> {

    ProgressDialog progressDialog;
    private Context context;
    private OnTaskCompleted done;
    private BatoipopCategoria categ;
    private ArrayList<BatoipopArticulo> listCateg = new ArrayList<>();

    public SearchByCategory(Context context, OnTaskCompleted done) {
        this.context = context;
        this.done = done;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Cargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            BatoipopArticuloDAO daoArt = new BatoipopArticuloDAO();
            listCateg = daoArt.findByCat(categ);
            done.onTaskCompleted();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();
    }

    public void setCateg(BatoipopCategoria categ) {
        this.categ = categ;
    }

    public ArrayList<BatoipopArticulo> getListCateg() {
        return listCateg;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
