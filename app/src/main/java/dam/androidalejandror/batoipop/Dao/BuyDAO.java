package dam.androidalejandror.batoipop.Dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.sql.SQLException;

import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;

public class BuyDAO  extends AsyncTask<String, String, String> {

    Context context;
    ProgressDialog progressDialog;
    private BatoipopArticulo articulo;
    private OnTaskCompleted done;
    

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Comprando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            BatoipopArticuloDAO artDAO = new BatoipopArticuloDAO();
            artDAO.update(articulo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setArticulo(BatoipopArticulo articulo) {
        this.articulo = articulo;
    }
}
