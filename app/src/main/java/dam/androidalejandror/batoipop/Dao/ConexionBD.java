package dam.androidalejandror.batoipop.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Patrón singleton
public class ConexionBD {

//    private static final String JDBC_URL = "jdbc:mariadb://192.168.56.101:3306/empresa_ad";
private static final String JDBC_URL = "jdbc:postgresql://137.74.226.41:5432/batoipop?currentSchema=public";

    private static Connection con = null;    

    public static Connection getConexion() throws SQLException {
        if (con == null) {
            Properties pc = new Properties();
            pc.put("user", "odoo");
            pc.put("password", "Windows2008Server");
            con = DriverManager.getConnection(JDBC_URL, pc);
        }
        return con;
    }

    public static void cerrar() throws SQLException {
        if (con != null) {
            con.close();
            con = null;
        }
    }

    public static void iniciaTransaccion() throws SQLException {
        con.setAutoCommit(false);
    }

    public static void confirmar() throws SQLException {
        con.commit();
        con.setAutoCommit(true);
    }

    public static void retroceder() throws SQLException {
        con.rollback();
        con.setAutoCommit(true);
    }
}
