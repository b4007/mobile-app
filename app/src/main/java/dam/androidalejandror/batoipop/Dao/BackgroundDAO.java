package dam.androidalejandror.batoipop.Dao;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import java.sql.SQLException;

import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;

public class BackgroundDAO extends AsyncTask<String, String, String> {

    private BatoipopArticulo art;
    ProgressDialog progressDialog;
    private Context context;
    private OnTaskCompleted done;

    public BackgroundDAO(Context context, OnTaskCompleted done) {
        this.context = context;
        this.done = done;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Subiendo...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            BatoipopArticuloDAO daoArt = new BatoipopArticuloDAO();
            daoArt.insert(art);
            done.onTaskCompleted();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();
    }

    public void setArt(BatoipopArticulo art) {
        this.art = art;
    }
}
