package dam.androidalejandror.batoipop.Dao;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.sql.SQLException;

import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Login;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;
import dam.androidalejandror.batoipop.Register;

public class BackgroundUsuarioDAO extends AsyncTask<String, String, String> {

    private BatoipopUsuario user;
    private String mail;
    private String pswd;
    private Context context;

    private OnTaskCompleted done;
    private int userID;

    public BackgroundUsuarioDAO(OnTaskCompleted done) {
        this.done = done;
    }

    @Override
    protected String doInBackground(String... strings) {
        BatoipopUsuarioDAO userDAO;
        try {
            userDAO = new BatoipopUsuarioDAO();
            userID = userDAO.canLogin(mail,pswd);
            done.onTaskCompleted();
        } catch (SQLException throwables) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    AlertDialog dialog = new AlertDialog.Builder(context)
                            .setMessage("¡Error al establecer la conexión! \nComprueba tu conexión a internet y vuelve a intentarlo de nuevo!")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
//
                                }
                            }).create();
                    dialog.show();
                }
            });
        }
        return null;
    }


    public void setUser(BatoipopUsuario user) {
        this.user = user;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPswd(String pswd) {
        this.pswd = pswd;
    }

    public int getUserID() {
        return userID;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
