package dam.androidalejandror.batoipop.Dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.sql.SQLException;
import java.util.ArrayList;

import dam.androidalejandror.batoipop.Api.OnTaskCompleted;
import dam.androidalejandror.batoipop.Pojos.BatoipopArticulo;
import dam.androidalejandror.batoipop.Pojos.BatoipopUsuario;

public class MyProductsDAO extends AsyncTask<String, String, String> {

    private BatoipopUsuario user;
    Context context;
    private int vendedor;
    private ArrayList<BatoipopArticulo> list;
    ProgressDialog progressDialog;

    private OnTaskCompleted done;

    public MyProductsDAO(OnTaskCompleted done) {
        this.done = done;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Cargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            BatoipopArticuloDAO artDAO = new BatoipopArticuloDAO();
            list = artDAO.findByNombre(vendedor);
            done.onTaskCompleted();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();
    }

    public void setVendedor(int vendedor) {
        this.vendedor = vendedor;
    }

    public ArrayList<BatoipopArticulo> getList() {
        return list;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
